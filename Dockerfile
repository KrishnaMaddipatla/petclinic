FROM alpine/git
WORKDIR /assignment_6
RUN git clone https://github.com/spring-projects/spring-petclinic.git

FROM maven:3.5-jdk-8-alpine
WORKDIR /assignment_6
COPY --from=0 /assignment_6/spring-petclinic /assignment_6
RUN mvn clean package

FROM openjdk:8-jre-alpine
WORKDIR /assignment_6
COPY --from=1 /assignment_6/target/*.jar .
CMD java -jar *.jar

EXPOSE 8080

